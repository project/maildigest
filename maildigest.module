<?php
/**
 * @file
 * Sends digest.
 */

/**
 * Implements hook_permission().
 */
function maildigest_permission() {
  return array(
    'administer maildigest' => array(
      'title' => t('Administer maildigest'),
      'description' => t('Perform administration tasks for maildigest.'),
    ),
  );
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function maildigest_ctools_plugin_directory($module, $plugin) {
  return 'plugins/' . $module . '/' . $plugin;
}

/**
 * Implements hook_form_alter().
 */
function maildigest_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'ctools_export_ui_delete_confirm_form') {
    $form['#submit'][] = '_maildigest_delete_submit';
  }
  elseif ($form_id == 'user_profile_form' || $form_id == 'user_register_form') {
      if ($form_id == 'user_profile_form') $uid = arg(1);
      else $uid = 0;
      $options = _maildigest_digest_load_all_as_options(FALSE);
      $checked_options = array_keys(_maildigest_load_subscriptions_as_options($uid));
      if (count($options)) {
        $form['maildigest'] = array(
          '#type' => 'fieldset',
          '#title' => t('Digests'),
          '#description' => t('Select the digest(s) to which you wish to subscribe.'),
          '#weight' => 5,
        );
        $form['maildigest']['digests'] = array(
          '#type' => 'checkboxes',
          '#options' => $options,
          '#default_value' => $checked_options,
        );
      }
  }
}

/**
 * Delete variables and subscriptions when a digest is removed.
 */
function _maildigest_delete_submit($form, &$form_state) {
  $digest_name = arg(4);
  $digest = maildigest_digest_load($digest_name);
  variable_del('maildigest_digest_last_sent_' . $digest_name);
  variable_del('maildigest_digest_end_time_' . $digest_name);
  db_delete('maildigest_subscription')
    ->condition('digest_name', $digest_name)
    ->execute();
}

/**
 * Wrapper to load any class type.
 */
function maildigest_plugin_load_class($module, $plugin, $type, $id, $args = NULL) {
  ctools_include('plugins');
  if ($class = ctools_plugin_load_class($module, $type, $plugin, 'handler')) {
    return new $class($args);
  }
}

/**
 * Load plugins.
 */
function maildigest_get_plugins($module, $type) {
  ctools_include('plugins');
  $plugins = ctools_get_plugins($module, $type);
  $result = array();
  $weights = array();
  foreach ($plugins as $key => $info) {
    if (!empty($info['hidden'])) {
      continue;
    }
    if (!isset($info['weight'])) {
      $info['weight'] = 10;
    }
    $weights[] = $info['weight'];
    $result[$key] = $info;
  }
  array_multisort($weights, $result);
  return $result;
}

/**
 * Load a digest.
 */
function maildigest_digest_load($digest) {
  ctools_include('export');
  return ctools_export_crud_load('maildigest_digest', $digest);
}

/**
 * Load all digests.
 */
function maildigest_digest_load_all($show_disabled = TRUE) {
  ctools_include('export');
  $digests = ctools_export_crud_load_all('maildigest_digest');
  foreach ($digests as $name => $digest) {
    if (isset($digest->disabled) && $digest->disabled && !$show_disabled) {
      unset($digests[$name]);
    }
  }
  return $digests;
}

/**
 * Load all digests as an associative array.
 */
function _maildigest_digest_load_all_as_options($show_disabled = TRUE) {
  $options = array();
  foreach (maildigest_digest_load_all($show_disabled) as $digest) {
    $options[$digest->name] = $digest->admin_title;
  }
  return $options;
}

/**
 * Load JavaScript for the form.
 */
function _maildigest_load_javascript_after_build_form($form, &$form_state) {
  drupal_add_js(drupal_get_path('module', 'maildigest') . '/maildigest.js');
  return $form;
}

/**
 * Return subscriptions as an associative array.
 */
function _maildigest_load_subscriptions_as_options($uid) {
  $options = array();
  foreach (_maildigest_get_subscriptions_for_user($uid) as $digest) {
    $options[$digest->name] = $digest->admin_title;
  }
  return $options;
}

/**
 * Get subscribers of a digest as users.
 */
function _maildigest_get_subscribers($name) {
  $subscribers = db_select('maildigest_subscription', 's')
    ->fields('s', array('uid'))
    ->condition('s.digest_name', $name)
    ->execute()
    ->fetchAll();
  $users = array();
  foreach ($subscribers as $subscriber) {
    $users[] = user_load($subscriber->uid);
  }
  return $users;
}

/**
 * Get subscriptions of a user as digest objects.
 */
function _maildigest_get_subscriptions_for_user($uid) {
  $subscriptions = db_select('maildigest_subscription', 's')
    ->fields('s', array('digest_name'))
    ->condition('s.uid', $uid)
    ->execute()
    ->fetchAll();
  $digests = array();
  foreach ($subscriptions as $subscription) {
    $digests[] = maildigest_digest_load($subscription->digest_name);
  }
  return $digests;
}

/**
 * Return whether a user is subscribed to a digest.
 */
function _maildigest_is_user_subscribed_to($uid, $name) {
  $count = db_select('maildigest_subscription', 's')
    ->condition('s.uid', $uid)
    ->condition('s.digest_name', $name)
    ->countQuery()
    ->execute()
    ->fetchField();
  if (intval($count) > 0) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Return whether a digest is disabled.
 */
function _maildigest_is_digest_disabled($name) {
  $digest = maildigest_digest_load($name);
  if ($digest) {
    return $digest->disabled;
  }
  else {
    return TRUE;
  }
}

/**
 * Unsubscribe a user from all digests.
 */
function _maildigest_unsubscribe_user_from_all($uid) {
  return db_delete('maildigest_subscription')
    ->condition('uid', $uid)
    ->execute();
}

/**
 * Subscribe a user to a digest.
 */
function _maildigest_subscribe_user($uid, $name) {
  if (_maildigest_is_user_subscribed_to($uid, $name) || _maildigest_is_digest_disabled($name)) {
    return FALSE;
  }
  else {
    return db_insert('maildigest_subscription')
      ->fields(array('uid' => $uid, 'digest_name' => $name))
      ->execute();
  }
}

/**
 * Unsubscribe a user from a digest.
 */
function _maildigest_unsubscribe_user($uid, $name) {
  if (_maildigest_is_user_subscribed_to($uid, $name)) {
    return db_delete('maildigest_subscription')
      ->condition('uid', $uid)
      ->condition('digest_name', $name)
      ->execute();
  }
  else {
    return FALSE;
  }
}

/**
 * Subscription form action.
 */
function _maildigest_process_form($field, $account) {
  if (isset($field)) {
    foreach ($field as $name => $checked) {
      if ($checked && _maildigest_subscribe_user($account->uid, $name)) {
        $digest = maildigest_digest_load($name);
        drupal_set_message(t('%name has been subscribed to %digest.', array('%name' => $account->name, '%digest' => $digest->admin_title)));
      }
      elseif (!$checked && _maildigest_unsubscribe_user($account->uid, $name)) {
        $digest = maildigest_digest_load($name);
        drupal_set_message(t('%name has been unsubscribed from %digest.', array('%name' => $account->name, '%digest' => $digest->admin_title)));
      }
    }
  }
}

/**
 * Implements hook_user_update().
 */ 
function maildigest_user_update(&$edit, $account, $category) {
  _maildigest_process_form($edit['digests'], $account);
}

/**
 * Implements hook_user_insert().
 */ 
function maildigest_user_insert(&$edit, $account, $category) {
  _maildigest_process_form($edit['digests'], $account);
}

/**
 * Implements hook_user_delete().
 */ 
function maildigest_user_delete($account) {
  _maildigest_unsubscribe_user_from_all($account->uid);
}

/**
 * Calculates start and end time variables for next digest mailing.
 */
function maildigest_digest_set_next_times($digest) {
  // Last end time timestamp when digest was sent last, or created time if never sent
  $last_time_sent = variable_get('maildigest_digest_last_sent_' . $digest->name, 0);

  $next_start_time = $last_time_sent;
  $frequency = (($digest->settings && $digest->settings['frequency']) ? $digest->settings['frequency'] : 'daily');
  $send_hour = (($digest->settings && $digest->settings['hour']) ? $digest->settings['hour'] : 0);
  $interval = 1;
  $timezone = variable_get('date_default_timezone', 0);
  $adjusted_last_time_sent = $last_time_sent + $timezone;
  if ($frequency == 'daily') {
    $next_day_to_send = gmmktime(0, 0, 0, gmdate("m", $adjusted_last_time_sent), gmdate('d', $adjusted_last_time_sent) + $interval, gmdate('Y', $adjusted_last_time_sent)); 
  }

  if ($frequency == 'weekly') {
    $weekday = $digest->settings['weekly'];
    $approx_date_to_send_next = $adjusted_last_time_sent + ($interval * 86400 * 7);
    $next_day_of_year_to_send = gmdate('z', $approx_date_to_send_next);
    $next_day_of_week_to_send = gmdate('w', $approx_date_to_send_next);
    if ($weekday > $next_day_of_week_to_send) {
      $next_day_of_year_to_send += $weekday - $next_day_of_week_to_send;
    }
    else {
      $next_day_of_year_to_send -= $next_day_of_week_to_send - $weekday;
    }
    $next_day_to_send = gmmktime(0, 0, 0, 1, $next_day_of_year_to_send+1, gmdate('Y', $adjusted_last_time_sent));
  }

  if ($frequency == 'monthly') {
    $day_of_month = $digest->settings['monthly']; 
    $last_month_sent = gmdate('n', $adjusted_last_time_sent);
    $next_month_to_send = $last_month_sent + $interval;
    if ($day_of_month == 'First day') {
      $next_day_to_send = gmmktime(0, 0, 0, $next_month_to_send, 1, gmdate('Y', $adjusted_last_time_sent));
    }
    elseif ($day_of_month == 'Last day') {
      $next_day_to_send = gmmktime(0, 0, 0, $next_month_to_send + 1, 0, gmdate('Y', $adjusted_last_time_sent));
    }
    elseif ($day_of_month == '15th') {
      $next_day_to_send = gmmktime(0, 0, 0, $next_month_to_send, 15, gmdate('Y', $adjusted_last_time_sent));
    }
    else {
      $day_of_month = _maildigest_get_day_of_month($next_month_to_send, $day_of_month, $adjusted_last_time_sent);
      $next_day_to_send = gmmktime(0, 0, 0, $next_month_to_send, $day_of_month, gmdate('Y', $adjusted_last_time_sent));
    }
  }
  $next_end_time = $next_day_to_send + $send_hour - $timezone - 1;
  variable_set('maildigest_digest_end_time_' . $digest->name, $next_end_time);
}

/**
 * Get day of month.
 */
function _maildigest_get_day_of_month($next_month_to_send, $day_of_month, $adjusted_last_time_sent) {
  $week = array(0 => 'First', 1 => 'Second', 2 => 'Third', 3 => 'Fourth');
  $weekdays = array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
  for ($week_of_month = 0; $week_of_month < 5; $week_of_month++) {
    for ($days_of_week = 0; $days_of_week < 7; $days_of_week++) {
      $day_index = ($week_of_month * 7) + $days_of_week + 1;
      $day = gmdate('w', gmmktime(0, 0, 0, $next_month_to_send, $day_index, gmdate('Y', $adjusted_last_time_sent)));
      if (($week[$week_of_month] . ' ' . $weekdays[$day]) == $day_of_month) {
        return ($day_index);
      }
    }
  }
}

/**
 * Send a digest. 
 */
function _maildigest_send_digest($digest) {
  global $language;
  $current_language = $language;
  $languages = language_list();
  foreach (_maildigest_get_subscribers($digest->name) as $user) {
    $lang = str_replace('-', '_', $user->language);
    $subject = !empty($digest->settings['subject_' . $lang]) ? $digest->settings['subject_' . $lang] : $digest->settings['subject_en'];
    $message = !empty($digest->settings['message_' . $lang]['value']) ? $digest->settings['message_' . $lang]['value'] : $digest->settings['message_en']['value'];
    $format = !empty($digest->settings['message_' . $lang]['format']) ? $digest->settings['message_' . $lang]['format'] : filter_fallback_format();
    $message = check_markup(token_replace($message, array('node' => $digest, 'user' => $user), array('clear' => TRUE)), $format, '', FALSE);
    $subject = token_replace($subject, array('node' => $digest, 'user' => $user));
    
    // Set both HTML and Plain
    $body_html = $message;
    $boundary = '----=_NextPart_' . md5(uniqid());
    $body_text = strip_tags(preg_replace('/<style>.+?<\/style>/ims', '', $body_html)); 
    $multi_body  = "This is a multi-part message in MIME format.

--$boundary
Content-Type: text/plain; charset=UTF-8; format=flowed; 
Content-Transfer-Encoding: 8bit

$body_text

--$boundary
Content-Type: text/html; charset=UTF-8; format=flowed; 
Content-Transfer-Encoding: 8bit

$body_html";
   
    $sender = (!empty($digest->settings['sender']) ? $digest->settings['sender'] : variable_get('maildigest_default_sender', ''));

    $content = array(
      'id' => 'maildigest_mail',
      'to' => $user->mail,
      'subject' => $subject,
      'body' => $multi_body,
      'headers' => array(
        //'MIME-Version' => '1.0',
        //'Content-Type' => 'multipart/alternative; boundary="'.$boundary.'"',
        //'Content-Transfer-Encoding' => '8Bit',
        'From' => $sender,
        'Sender' => $sender,
        'Return-Path' => $sender,
      )
    );
    
    $system = drupal_mail_system('maildigest', 'mail');
    $system->mail($content);
  }
  variable_set('maildigest_digest_last_sent_' . $digest->name, time());
  maildigest_digest_set_next_times($digest);
  $language = $current_language;
}

/**
 * Implements hook_cron(). 
 */
function maildigest_cron() {
  $current_time = time();
  foreach (maildigest_digest_load_all(FALSE) as $digest) {
    $send_time = variable_get('maildigest_digest_end_time_' . $digest->name, 0);
    if ($send_time && $current_time > $send_time) {
      _maildigest_send_digest($digest);
    }
  }
}
